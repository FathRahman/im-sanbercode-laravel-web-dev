<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);

Route::post('/home', [HomeController::class, 'welcome']);  

Route::get('/data-table', function(){
    return view('page.datatable');
});

Route::get('/tables', function(){
    return view('page.table');
});

//CRUD CAST CAST

    // CREATE DATA CAST
        //Route to form inputdata {Add Data}
Route::get('/cast/create', [CastController::class, 'create']);
        //Route Save Data to database
Route::post('/cast', [CastController::class, 'store']);

    // READ DATA  CAST
        //Route for show all data from database(table Cast) to page
Route::get('/cast', [CastController::class, 'index']);
        //Route detail data berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);
    // UPDATE DATA CAST
        //Route ke halaman form update berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
        //Dimpan Update Data ke database berdasarkan id
Route::put('/cast/{id}',[CastController::class,'update']);

    // DROP DATA CAST
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
