@extends('layouts.master')
@section('title')
    Halaman Register
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/home" method="POST">
        @csrf
        <label for="">First Name:</label><br><br>
            <input type="text" name="fname" id=""><br><br>
        <label for="">Last Name:</label><br><br>
            <input type="text" name="lname" id=""><br><br>
        <label for="">Gender:</label><br><br>
            <input type="radio" value="1" name="gender">Male<br>
            <input type="radio" value="1" name="gender">Female<br>
            <input type="radio" value="1" name="gender">Other<br><br>
        <label for="">Nationalityt:</label><br><br>
            <select name="nationalityts" id="">
                <option value="1">Indonesian</option>
                <option value="2">Arabian</option>
                <option value="4">Chinese</option>
                <option value="5">Indian</option>
                <option value="8">British</option>
                <option value="11">Italian</option>
                <option value="12">Spanish</option>
                <option value="13">Russian</option>
                <option value="">Other</option>
            </select><br><br>
        <label for="">langeuage Spoken:</label><br><br>
            <input type="checkbox" value="Bahasa Indonesia" name="bhsind">Bahasa Indonesia<br>
            <input type="checkbox" value="Bahasa Inggris" name="bhseng">Bahasa Inggris<br>
            <input type="checkbox" value="Other" name="other">Other<br><br>
        <label for="">Bio:</label><br><br>
            <textarea name="txt_bio" id="" cols="30" rows="10"></textarea><br>
        <button type="submit" value="kirim">Sign Up</button>
    </form>
@endsection