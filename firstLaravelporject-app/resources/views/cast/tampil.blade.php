@extends('layouts.master')
@section('title')
    Tampil Data
@endsection

@section('content')

<a href="/cast/create" class="btn btn-outline-success">Tambah Data</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Usia</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <th scope="row">{{$key+1}}</th>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
                                    {{-- variabel item - (id Database) --}}
            <td>
                
                <form action="/cast/{{$item->id}}" method="POSt">
                @csrf
                @method('delete')
                    <a href="/cast/{{ $item->id }}" class="btn btn-outline-info">Detail</a>
                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-outline-warning">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-outline-danger">
                </form>
            </td>
           
          </tr>
        @empty
            <tr><td>tidak ada data</td></tr>
        @endforelse
    </tbody>
  </table>

@endsection