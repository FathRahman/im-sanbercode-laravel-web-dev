@extends('layouts.master')
@section('title')
    Halaman Register
@endsection

@section('content')

{{-- eror Validasi start --}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{-- eror Validasi end --}}

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="txt_nama" class="form-control"  placeholder="Masukkan Nama">
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="number"name="txt_umur" class="form-control" >
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" name="txt_bio" rows="3"></textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection