<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('welcome');
    }

    public function welcome(Request $request){
        $namaDepan = $request->input('fname');
        $namaBelakang = $request->input('lname');
        return view('page.home',["namaDepan" => $namaDepan,"namaBelakang" =>$namaBelakang ]);
    }
}
