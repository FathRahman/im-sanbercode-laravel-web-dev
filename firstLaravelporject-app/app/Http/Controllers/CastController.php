<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.adddata');
    }

    public function store(Request $request)
    {
        $request->validate([
            // name => Aksi
            'txt_nama' => 'required',
            'txt_umur' => 'required|max:3',
            'txt_bio' => 'required',
        ]);

        DB::table('cast')->insert([
            //nama column di database = > request->(nama textfeild)
            'nama' => $request->input('txt_nama'),
            'umur' => $request->input('txt_umur'),
            'bio' => $request->input('txt_bio'),
        ]);

        return redirect('/cast');
    }
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.tampil',['cast' => $cast]);
    }

    //show data per id
    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        
        return view('cast.detail',['cast'=>$cast]);
    }

    //function edit data per-id
    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);

        return view('cast.edit', ['cast'=>$cast]);
    }

    //function update
    public function update($id, Request $request) 
    {
        $request->validate([
            // name => Aksi
            'txt_nama' => 'required',
            'txt_umur' => 'required|max:3',
            'txt_bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update([
                'nama' => $request->input('txt_nama'),
                'umur' => $request->input('txt_umur'),
                'bio'  => $request->input('txt_bio'),
            ]);

        return redirect('/cast'); 
    }

    //function destroy data ke database per - id
    public function destroy($id)
    {   
        //DB::table('tabel?')->where('id', '=', id)->delete();
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
