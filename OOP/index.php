<?php 

// impor class-nya "Animal"
require_once('animal.php');
require_once('Frog.php');
require_once('ape.php');

//wujud dari class yg di animal.php
$sheep = new Animal("shaun");

//Output
echo "Name Animal   : " . $sheep->name . "<br>"; // "shaun"
echo "Legs          : " . $sheep->legs . "<br>"; // 4"
echo "Cold Blood    : " . $sheep->cold_blooded . "<br><br>"; // "no"


$kodok = new Frog("buduk");

echo "Name Animal   : " . $kodok->name . "<br>";
echo "Legs          : " . $kodok->legs . "<br>";
echo "Cold Blood    : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump();//Dari Function Frog

$sungokong = new Ape("kera sakti");

echo "Name Animal   : " . $sungokong->name . "<br>";
echo "Legs          : " . $sungokong->legsape . "<br>";
echo "Cold Blood    : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell();//Dari Function Ape

?>